# Commands

## test

```sh
swipl -g run_tests -t halt src/*_test.plt
```

### test watch

```sh
watchexec --clear -- $MASK test
```

## run

OPTIONS

- NAME
  - flags: --name
  - type: string

```sh
if [ -n "$NAME" ]; then
  ./src/hello.pl "$NAME"
else
  ./src/hello.pl
fi
```

## update

```bash
set -o errexit
set -o nounset
set -o pipefail

nix flake update
direnv exec . \
    nix develop --command \
        devbox update
direnv exec . \
    nix develop '.#update_vscode_settings' --command \
        sd '"prolog.executablePath": ".*swipl"' "\"prolog.executablePath\": \"$(which swipl)\"" .vscode/settings.json
```

---

<!-- markdownlint-disable-next-line MD039 MD045 -->
This folder has been setup from the [`nix-sandboxes`'s template ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes)
