:- begin_tests(hello).

:- use_module('hello').

test('hello world') :-
    assertion(hello("Hello world", [])).

test('hello foo') :-
    assertion(hello("Hello foo", ["foo"])).

:- end_tests(hello).
