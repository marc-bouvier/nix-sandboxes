import Test.Hspec

type Name = Maybe String

hello :: Name -> String
hello (Just name) = "Hello " ++ name
hello Nothing = hello (Just "world")

main :: IO ()
main =
    hspec $ do
        describe "Hello" $ do
            it "world" $ do
                hello Nothing `shouldBe` "Hello world"

            it "foo" $ do
                hello (Just "foo") `shouldBe` "Hello foo"
