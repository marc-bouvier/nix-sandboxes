#!/usr/bin/env sh

. ./src/hello.sh

test_hello_world() {
	assertEquals "$(hello)" "Hello world"
}

test_hello_foo() {
	assertEquals "$(hello 'foo')" "Hello foo"
}
