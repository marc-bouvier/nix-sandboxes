# Small wrapper for snapshot testing

Also known as [Golden Master Testing](https://en.wikipedia.org/wiki/Software_testing#Output_comparison_testing)

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#WORKDIR=snapshot_testing/https://gitlab.com/pinage404/nix-sandboxes)

Or with [Nix](https://nixos.org)

```sh
NIX_CONFIG="extra-experimental-features = flakes nix-command" \
nix flake new --template "gitlab:pinage404/nix-sandboxes#snapshot_testing" ./your_new_project_directory
```

In `.envrc` change the `SNAPSHOT_TEST_COMMAND` with your actual test command

```sh
export SNAPSHOT_TEST_COMMAND="sh ./bin/hello" # change this
```

---

[Available commands](./maskfile.md)

Or just execute

```sh
mask help
```

---

[Amesome Testing](https://github.com/TheJambo/awesome-testing#readme)
