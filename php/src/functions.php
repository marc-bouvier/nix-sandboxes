<?php

declare(strict_types=1);

namespace App;

function hello(string $name = 'world'): string
{
    return "Hello $name";
}
