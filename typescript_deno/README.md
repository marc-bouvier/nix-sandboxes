# TypeScript with formatting, linting and test on Deno

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#WORKDIR=typescript_deno/https://gitlab.com/pinage404/nix-sandboxes)

Or with [Nix](https://nixos.org)

```sh
NIX_CONFIG="extra-experimental-features = flakes nix-command" \
nix flake new --template "gitlab:pinage404/nix-sandboxes#typescript_deno" ./your_new_project_directory
```

---

[Available commands](./maskfile.md)

Or just execute

```sh
mask help
```

---

[Awesome Deno](https://github.com/denolib/awesome-deno)

[Awesome Node.JS](https://github.com/sindresorhus/awesome-nodejs#contents) not
Deno but still accurate

[Awesome JavaScript](https://github.com/sorrycc/awesome-javascript#readme)
