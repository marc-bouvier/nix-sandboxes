# Python with formatting, typing and test using Poetry

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#WORKDIR=python_poetry/https://gitlab.com/pinage404/nix-sandboxes)

Or with [Nix](https://nixos.org)

```sh
NIX_CONFIG="extra-experimental-features = flakes nix-command" \
nix flake new --template "gitlab:pinage404/nix-sandboxes#python_poetry" ./your_new_project_directory
```

---

[Available commands](./maskfile.md)

Or just execute

```sh
mask help
```

---

[Awesome Python](https://github.com/vinta/awesome-python#readme)

[Awesome Python Typing](https://github.com/typeddjango/awesome-python-typing#readme)
